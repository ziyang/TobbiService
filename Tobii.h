#pragma once
#include "stdafx.h"
#include<WINSOCK2.H>
#include<iostream>
#include <tobii/tobii_streams.h>
#include <stdio.h>
#include <assert.h>
#include <thread>  
#include <mutex>
#include <sstream>
#include<deque>
#include <functional>
#include <condition_variable>
#include "Singleton.h"

#pragma comment(lib,"WS2_32.lib")

using namespace std;



//两只眼睛单独的3D坐标
struct OriginPoint {
	float leftEye[3];
	float rightEye[3];
};
//gaze_point的2D坐标
struct GazePoint {
	float gazePoint[2];
};
struct Eye {
	OriginPoint originPoint;
	GazePoint gazePoint;
};
std::mutex datamtx;
OriginPoint originPoint;
GazePoint gazePointXy;
Eye eye;
deque<Eye> eyeDeque;

std::mutex mtx;
std::condition_variable cv;

class Tobii {
	float leftEye[3];
	float rightEye[3];
	float gazePoint[2];
	void getEyePosition();
	void fetchDataFromBuffer();
public:
	int startService();
	std::function<void(float origin_gaze_left_x,float origin_gaze_left_y, float origin_gaze_left_z, float origin_gaze_right_x, float origin_gaze_right_y, float origin_gaze_right_z, float origin_point_x, float origin_point_y)> asyncHandle;
	static void sendToBuffer(Eye eye);
	void registService(std::function<void(float origin_gaze_left_x, float origin_gaze_left_y, float origin_gaze_left_z, float origin_gaze_right_x, float origin_gaze_right_y, float origin_gaze_right_z, float origin_point_x, float origin_point_y)> rxHandler);
};
void Tobii::registService(std::function<void(float origin_gaze_left_x, float origin_gaze_left_y, float origin_gaze_left_z, float origin_gaze_right_x, float origin_gaze_right_y, float origin_gaze_right_z, float origin_point_x, float origin_point_y)> rxHandler) {
	asyncHandle = rxHandler;
}
void gaze_origin_callback(tobii_gaze_origin_t const* gaze_origin, void* user_data)
{
	if (gaze_origin->left_validity == TOBII_VALIDITY_VALID) {
		::originPoint.leftEye[0] = gaze_origin->left_xyz[0];
		::originPoint.leftEye[1] = gaze_origin->left_xyz[1];
		::originPoint.leftEye[2] = gaze_origin->left_xyz[2];
		printf("Left: %f, %f, %f ", gaze_origin->left_xyz[0], gaze_origin->left_xyz[1], gaze_origin->left_xyz[2]);
	}
	if (gaze_origin->right_validity == TOBII_VALIDITY_VALID) {
		::originPoint.rightEye[0] = gaze_origin->right_xyz[0];
		::originPoint.rightEye[1] = gaze_origin->right_xyz[1];
		::originPoint.rightEye[2] = gaze_origin->right_xyz[2];
		printf("Right: %f, %f, %f ", gaze_origin->right_xyz[0], gaze_origin->right_xyz[1], gaze_origin->right_xyz[2]);
	}
	printf("\n");
}

void gaze_point_callback(tobii_gaze_point_t const* gaze_point, void* user_data)
{
	if (gaze_point->validity == TOBII_VALIDITY_VALID) {
		::gazePointXy.gazePoint[0] = gaze_point->position_xy[0];
		::gazePointXy.gazePoint[1] = gaze_point->position_xy[1];
		printf("Gaze point: %f, %f\n", gaze_point->position_xy[0], gaze_point->position_xy[1]);
	}
	eye.gazePoint = ::gazePointXy;
	eye.originPoint = ::originPoint;
	Tobii::sendToBuffer(eye);
}
//获取两只眼睛的数据（3D和gaze_point数据），放到buffer中
void Tobii::getEyePosition()
{
	tobii_api_t* api;
	tobii_error_t error = tobii_api_create(&api, NULL, NULL);
	assert(error == TOBII_ERROR_NO_ERROR);
	tobii_device_t* device;
	error = tobii_device_create(api, NULL, &device);
	assert(error == TOBII_ERROR_NO_ERROR);
	//获取眼睛3D坐标
	error = tobii_gaze_origin_subscribe(device, gaze_origin_callback, 0);
	assert(error == TOBII_ERROR_NO_ERROR);
	//获取眼睛注视屏幕2D坐标
	error = tobii_gaze_point_subscribe(device, gaze_point_callback, 0);
	assert(error == TOBII_ERROR_NO_ERROR);
	while (true) {
		error = tobii_wait_for_callbacks(device);
		assert(error == TOBII_ERROR_NO_ERROR || error == TOBII_ERROR_TIMED_OUT);
		error = tobii_process_callbacks(device);
		assert(error == TOBII_ERROR_NO_ERROR);
	}
	//取消眼睛3D坐标订阅
	error = tobii_gaze_origin_unsubscribe(device);
	assert(error == TOBII_ERROR_NO_ERROR);
	//取消眼睛注视屏幕2D坐标订阅
	error = tobii_gaze_point_unsubscribe(device);
	assert(error == TOBII_ERROR_NO_ERROR);
	error = tobii_device_destroy(device);
	assert(error == TOBII_ERROR_NO_ERROR);
	error = tobii_api_destroy(api);
	assert(error == TOBII_ERROR_NO_ERROR);
}
//从buffer取数，执行用户定义的满足asyncHandle函数签名的方法
void Tobii::fetchDataFromBuffer() {
	std::unique_lock<std::mutex> lck(mtx);
	Eye eye;
	while (true) {
		cv.wait(lck);
		while (!::eyeDeque.empty()) {
			eye = ::eyeDeque.back();
			::eyeDeque.pop_back();
			Tobii::asyncHandle(eye.originPoint.leftEye[0], eye.originPoint.leftEye[1], eye.originPoint.leftEye[2], eye.originPoint.rightEye[0], eye.originPoint.rightEye[1], eye.originPoint.rightEye[2], eye.gazePoint.gazePoint[0], eye.gazePoint.gazePoint[1]);
			break;
		}
	}
}
void Tobii::sendToBuffer(Eye eye) {
	std::unique_lock<std::mutex> lck(mtx);
	while (eyeDeque.size() > 10) {
		eyeDeque.pop_front();
	}
	eyeDeque.push_back(eye);
	cv.notify_all();
}
int Tobii::startService()
{
	std::thread th1(&Tobii::getEyePosition, this);
	std::thread th2(&Tobii::fetchDataFromBuffer, this);
	th1.join();
	th2.join();
	return 0;
}
typedef Magic_Singleton<Tobii> TobiiServiceSingleton;
