#include "stdafx.h"
#include "Tobii.h"
#include <boost/asio.hpp>
#include <boost\thread.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/algorithm/string.hpp>
#include <WINSOCK2.H>

using namespace boost::property_tree;
using namespace boost::gregorian;
using namespace boost::asio::ip;
using namespace boost;

boost::asio::io_service ios;
//用户定义的满足asyncHandle的函数签名的函数,本例中为取数并通过UDP发送(JSON格式)
void sendUdp(float origin_gaze_left_x, float origin_gaze_left_y, float origin_gaze_left_z, float origin_gaze_right_x, float origin_gaze_right_y, float origin_gaze_right_z, float origin_point_x, float origin_point_y) {
	WSADATA wsaData;
	WORD wVersionRequested = MAKEWORD(1, 0);
	WSAStartup(wVersionRequested, &wsaData);

	SOCKET socket = ::WSASocket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, nullptr, 0, 0);
	if (socket == INVALID_SOCKET) {
		printf("WSASocket failed, error=%d\n", WSAGetLastError());
		return;
	}

	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(9999);  // 接收端端口
	addr.sin_addr.s_addr = inet_addr("127.0.0.1"); // 接收端IP

	stringstream output;
	ptree root, leftEye, rightEye, gazePoint;
	leftEye.put<float>("x", origin_gaze_left_x);
	leftEye.put<float>("y", origin_gaze_left_y);
	leftEye.put<float>("z", origin_gaze_left_z);
	rightEye.put<float>("x", origin_gaze_right_x);
	rightEye.put<float>("y", origin_gaze_right_y);
	rightEye.put<float>("z", origin_gaze_right_z);
	gazePoint.put<float>("gazeX", origin_point_x);
	gazePoint.put<float>("gazeY", origin_point_y);
	root.put_child("left", leftEye);
	root.put_child("right", leftEye);
	root.put_child("gaze", gazePoint);
	boost::property_tree::write_json(output, root);
	//sendto(socket, output.str().c_str(), output.str().size(), 0, (SOCKADDR *)&RecvAddr, sizeof(RecvAddr));
	sendto(socket, output.str().c_str(), output.str().size(), 0, reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
}

int main() {
	auto t = TobiiServiceSingleton::GetInstance();
	t->registService(sendUdp);
	t->startService();
}